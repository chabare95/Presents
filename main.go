package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"

	"github.com/chabare/Presents/present"
	"github.com/chabare/SQLGolang/cSql"
	mysql "github.com/ziutek/mymysql/mysql"
)

func contains(arr *[]string, str string) bool {
	for _, ele := range *arr {
		if ele == str {
			return true
		}
	}

	return false
}

func main() {
	// Table names for the database (ID is set to auto)
	recipient := cSql.DBTable{"Recipients", []string{"ID", "FirstName", "LastName"}}
	presents := cSql.DBTable{"Presents", []string{"Brand", "Product", "Store", "Cost", "RecipientID"}}
	find := contains(&os.Args, "-f")
	// Create an entry to be used in the database
	var entry present.Entry
	// Get recipient and present from user
	entry.Recipient = *readRecipient()
	if !find {
		entry.Present = *readPresent()
	}

	cff := cSql.ConfigFile{"config.ini"}
	conn := cSql.CreateConnectionByConfig(cff.GetConfig())
	if find {
		for _, ele := range getRecipientPresents(conn, &entry.Recipient, &recipient, &presents) {
			fmt.Printf("%s ", ele.Str(0))
			fmt.Printf("%s ", ele.Str(1))
			fmt.Printf("%s ", ele.Str(2))
			fmt.Printf("%v€\n", ele.Float(3))
		}
		os.Exit(0)
	}
	// Check if Recipient is already in database
	rInDB, rID := recipientInDatabase(conn, &entry.Recipient, &recipient)
	if !rInDB {
		rID = insertRecipientInDatabase(conn, &entry.Recipient, &recipient)
	}
	entry.Present.RecipientID = rID

	insertPresentInDatabase(conn, &entry.Present, &presents)
}

func recipientInDatabase(conn *mysql.Conn, recipient *present.Recipient, recipientTable *cSql.DBTable) (bool, int) {
	conditions := []cSql.Condition{cSql.Condition{recipientTable.Columns[1], recipient.FirstName}, cSql.Condition{recipientTable.Columns[2], recipient.LastName}}
	rows, _ := recipientTable.Select(conn, &recipientTable.Columns, &conditions)

	id := -1
	if len(rows) > 0 {
		id = rows[0].Int(0)
	}

	return len(rows) > 0, id
}

func insertRecipientInDatabase(conn *mysql.Conn, recipient *present.Recipient, recipientTable *cSql.DBTable) int {
	recWoID := recipientTable.Columns[1:]

	res := recipientTable.Insert(conn, &recWoID, recipient.ToStringArray())

	return int(res.InsertId())
}

func insertPresentInDatabase(conn *mysql.Conn, present *present.Present, presentTable *cSql.DBTable) {
	presentTable.Insert(conn, &presentTable.Columns, present.ToStringArray())
}

func getRecipientPresents(conn *mysql.Conn, recipient *present.Recipient, recipientTable *cSql.DBTable, presentTable *cSql.DBTable) []mysql.Row {
	_, recID := recipientInDatabase(conn, recipient, recipientTable)
	if recID < 0 {
		log.Fatal("Recipient isn't in the database.")
	}
	conditions := []cSql.Condition{cSql.Condition{presentTable.Columns[4], strconv.FormatInt(int64(recID), 10)}}

	colWoRecID := presentTable.Columns[:len(presentTable.Columns)-1]
	rows, _ := presentTable.Select(conn, &colWoRecID, &conditions)

	return rows
}

func readRecipient() *present.Recipient {
	var r present.Recipient
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Enter the name of the recipient.(With a space between first and last name)")
	input, _ := reader.ReadString('\n')

	r.FirstName = strings.Split(input, " ")[0]
	r.LastName = strings.Split(input, " ")[1]

	return &r
}

func readPresent() *present.Present {
	var p present.Present
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Enter the brand and name of the product")
	input, _ := reader.ReadString('\n')

	p.Brand = strings.Split(input, " ")[0]
	// Read everything behind brand as product name
	p.Name = strings.Join(strings.Split(input, " ")[1:], " ")
	p.Name = strings.TrimSuffix(strings.Split(input, " ")[1], "\n")

	fmt.Println("Enter the store and cost of the product")
	input, _ = reader.ReadString('\n')
	p.Store = strings.Split(input, " ")[0]
	cost, err := strconv.ParseFloat(strings.TrimSuffix(strings.Split(input, " ")[1], "\n"), 32)

	if err != nil {
		log.Fatalf("There was an error with your cost input: %v\n", err)
	}

	p.Cost = round(cost, .5, 2)

	return &p
}

func round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}
