package present

import "strconv"

// Entry is a struct which connects the Present and it's Recipient
type Entry struct {
	Present
	Recipient
}

// Present represents a present with it's various attributes
type Present struct {
	Brand       string
	Name        string
	Store       string
	Cost        float64
	RecipientID int
}

// Recipient represents a Recipient of a present with it's first and last name
type Recipient struct {
	FirstName string
	LastName  string
}

// ToStringArray converts the recipient struct to a string array
func (r *Recipient) ToStringArray() *[]string {
	return &[]string{r.FirstName, r.LastName}
}

// ToStringArray converts the presents struct to a string array
func (p *Present) ToStringArray() *[]string {
	return &[]string{p.Brand, p.Name, p.Store, strconv.FormatFloat(p.Cost, 'f', 6, 64), strconv.FormatInt(int64(p.RecipientID), 10)}
}
